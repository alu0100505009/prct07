module Prct06

	class Bibliografia
		attr_reader :referencias
		attr_accessor :head

		Node = Struct.new(:value, :next)

		def initialize(referencias)
			if referencias.instance_of? Referencia
				@head = Node.new(referencias, nil)
				@head.next = nil
			else raise "Debe crearse una clase referencia" end
		end

		def << (value)
			if value.instance_of? Referencia
				if @head == nil
					@head = Node.new(value, nil)
				elsif
					aux = @head
					while aux.next != nil
						aux=aux.next
					end
					aux.next = Node.new(value, nil)
				end					
			elsif value.instance_of? Array
				value.each do |i|
					self << (i)
			end
			else raise "No sabemos que has pasado" end
		end

		def del
			@head = @head.next
		end
	end

end
