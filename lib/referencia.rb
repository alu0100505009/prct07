module Prct06
	
	class Referencia
		attr_accessor :autor, :titulo, :serie, :editorial, :num_edi, :fecha_publi, :isbn

		def initialize (autor, titulo, serie, editorial, num_edi, fecha_publi, isbn)
			@autor = autor
			@titulo = titulo
			@serie = serie
			@editorial = editorial
			@num_edi = num_edi
			@fecha_publi = fecha_publi
		   @isbn = isbn
		end

		def to_s
			salida = String.new ""
			salida += self.autores
			salida +=
			"#{titulo}\n"+
			"#{serie}\n"+
			"#{editorial}; #{num_edi} (#{fecha_publi})\n"
			salida += self.isbns
			return salida
		end

		def autores
			salida = String.new ""
			if @autor.instance_of? String
				salida = "#{autor}\n"
			elsif @autor.instance_of? Array
				for item in @autor
					if @autor.last == item
						salida += item.to_s+"\n"
					else
						salida += item.to_s+", "
					end
				end
			end
			return salida
		end


		def isbns
			salida = String.new ""
			if @isbn.instance_of? String
				salida = "#{isbn}\n"
			elsif @isbn.instance_of? Array
				for item in @isbn
				salida += item.to_s+"\n"
				end
			end
			return salida
		end
		
		
	end


end
